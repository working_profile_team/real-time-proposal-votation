Template.systemStatus.helpers({
    lastStatusType: function () {
        var lastSystemStatus = SystemStatus.findOne({}, {sort: {createdAt: -1}, limit: 1});
        return SystemStatusType.findOne({_id: lastSystemStatus.statusTypeId});
    },
    lastStatusHasDuration: function () {
        var lastSystemStatus = SystemStatus.findOne({}, {sort: {createdAt: -1}, limit: 1});
        return lastSystemStatus.hasDuration;
    },
    daysLeft: function () {
        var lastSystemStatus = SystemStatus.findOne({}, {sort: {createdAt: -1}, limit: 1});
        var currentDate = new Date();
        var deltaInSeconds = lastSystemStatus.endsAt - currentDate;
        if (deltaInSeconds < 0)
            deltaInSeconds = 0;
        var deltaInDays = Math.ceil(deltaInSeconds / (1000*60*60*24));
        return deltaInDays;
    },
});
