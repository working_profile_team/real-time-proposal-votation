Template.proposalList.helpers({
    proposals: function () {
        if (!this._sort)
            this._sort = {createdAt: -1};
        return Proposals.find({}, {sort: this._sort});
    },
});