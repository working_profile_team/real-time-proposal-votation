
Template.proposalEdit.helpers({
    proposal: function() {
        return Proposals.findOne(Session.get('currentProposalId'));
    }
});

Template.proposalEdit.events({
    'submit form': function(e) {
        e.preventDefault();
        var currentProposalId = Session.get('currentProposalId');
        var proposalAttrs = {
            title: $(e.target).find('#proposal-title-id').val(),
            description: $(e.target).find('#proposal-description').val(),
            articles: $(e.target).find('#proposal-edit-articles textarea[name="article-description"]').map(function () {
                return {description: $(this).val()}
            }).get()
        }
        Meteor.call('proposalEdit', currentProposalId, proposalAttrs, function(error, id) {
            if (error) {
                // display the error to the user
                Errors.throw(error.reason);
                // if the error is that the proposal already exists, take us there
                if (error.error === 302)
                    Router.go('wall', error.details)
            } else {
                Router.go('wall');
            }
        });
    },
    'click button[name=proposal-edit-btn-add-article]': function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(e.target).closest('form').find('#proposal-edit-last-article').before(Meteor.render(function () {
            return Template['articleAdd']()
        }));
        Animations.scrollToBottom(1500);
        $('form textarea:last').focus();
    }
});
