
Template.proposalRead.helpers({
    proposal: function () {
        var currentProposalId = Session.get('currentProposalId');
        return Proposals.findOne(currentProposalId);
    },
    articles: function () {
        return Articles.find({proposalId: this._id}, {sort: {articleNumber: 1}})
    },
    ownProposal: function () {
        return this.userId === Meteor.userId();
    },
});
