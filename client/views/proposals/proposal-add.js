
Template.proposalAdd.events({
    'submit form': function(e) {
        e.preventDefault();
        var proposal = {
            title: $(e.target).find('#proposal-title-id').val(),
            description: $(e.target).find('#proposal-description').val(),
            articles: $(e.target).find('#proposal-add-articles textarea[name="article-description"]').map(function () {
                return {description: $(this).val()}
            }).get()
        };
        Meteor.call('proposalAdd', proposal, function(error, id) {
            if (error) {
                // display the error to the user
                Errors.throw(error.reason);
            } else {
                Router.go('proposalList');
            }
        });
    },
    'click button[name=proposal-add-btn-add-article]': function (e) {
        e.preventDefault();
        e.stopPropagation();

        $(e.target).closest('form').find('#proposal-add-last-article').before(Meteor.render(function () {
            return Template['articleAdd']()
        }));
        Animations.scrollToBottom(1500);
        $('form textarea:last').focus();
    }
});