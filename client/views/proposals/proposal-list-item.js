Template.proposalListItem.events({
    'click .delete': function(e) {
        e.preventDefault();
        var currentProposalId = this._id;
        if (confirm("¿Desea borrar esta propuesta?")) {
            Meteor.call('proposalDelete', currentProposalId, function(error, id) {
                if (error) {
                    // display the error to the user
                    Errors.throw(error.reason);
                    // if the error is that the proposal already exists, take us there
                    if (error.error === 302)
                        Router.go('wall', error.details)
                } else {
                    Router.go('wall');
                }
            });
        }
    },
});

Template.proposalListItem.helpers({
    articles: function () {
        return Articles.find({proposalId: this._id}, {sort: {articleNumber: 1}, limit: 3})
    },
    allArticlesLoaded: function () {
        return Articles.find({proposalId: this._id}).count() < 3;
    },
    canEditProposal: function () {
        return Permissions.Proposals.canEditProposal(this._id);
    }
});
