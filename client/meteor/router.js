//--------------------------------------------------------------------------------------------------//
//------------------------------------------- Controllers ------------------------------------------//
//--------------------------------------------------------------------------------------------------//


ProposalController = RouteController.extend({
    before: function () {
        var route = this.route.name;
        if (route == 'proposalEdit'){
            var proposalId = this.params._id;
            var hasPermission = Permissions.Proposals.canEditProposal(proposalId);
            if (! hasPermission)
                Router.go('noRights');
            Session.set('currentProposalId', proposalId);
        }
        if (route == 'proposalRead') {
            Session.set('currentProposalId', this.params._id);
        }
    },
    data: function () {
        var route = this.route.name;
        return {templateName: route}
    }
});

chooseProposalController = RouteController.extend({
    before: function () {
        var route = this.route.name;
        if (route == 'proposalEdit') {
            var proposalId = this.params._id;
            var hasPermission = Permissions.canEditProposal(proposalId);
            if (!hasPermission)
                Router.go('noRights');
            Session.set('currentProposalId', proposalId);
        }
        if (route == 'chooseProposalRead') {
            Session.set('currentProposalId', this.params._id);
        }
    },
    data: function () {
        var route = this.route.name;
        return {templateName: route}
    }
});

LoginController = RouteController.extend({
    redirectOnLogin: true,
    before: function () {
        var currentUser = Meteor.user();
        if (currentUser){
            Meteor.call('loginUpdateLastLoginAt', function () {
                Meteor.subscribe("userData");
            });
            Meteor.call('updateDiscussAvailableVotes');
            this.redirect('wall');
        }
    },
});

//--------------------------------------------------------------------------------------------------//
//------------------------------------------- Routes -----------------------------------------------//
//--------------------------------------------------------------------------------------------------//


Router.map(function () {

    this.route('notFound', {});

    this.route('noRights', {});

    this.route('login', {
        controller: LoginController
    });

    this.route('wall', {
        path: '/',
        loginRequired: 'login',
        action: function () {
            this.template = 'proposalList';
            var lastSystemStatus = SystemStatus.findOne({}, {sort: {createdAt: -1}, limit: 1});
            if (lastSystemStatus.statusTypeCode == 1)
                this.template = 'chooseProposalListMostRecent';
            if (lastSystemStatus.statusTypeCode == 2)
                this.template = 'discussProposalListItem';
            this.render();
        },
    });
    //--------------------------------------------------------------------------------------------------//
    // -------------------------------------------- Proposals ----------------------------------------- //

    // -------------------------------------------- General ------------------------------------------- //
    // -------- proposalAdd -------- //
    this.route('proposalAdd', {
        path: '/propuestas/agregar',
        loginRequired: 'login',
        controller: ProposalController
    });

    // -------- proposalEdit -------- //
    this.route('proposalEdit', {
        path: '/propuesta/:_id/editar',
        loginRequired: 'login',
        controller: ProposalController
    });

    // ----------------------------------------- proposalList ----------------------------------------- //
    this.route('proposalList', {
        path: '/propuestas/',
        loginRequired: 'login',
        controller: ProposalController
    });

    // -------- proposalRead -------- //
    this.route('proposalRead', {
        path: '/propuesta/:_id/leer',
        loginRequired: 'login',
        controller: ProposalController
    });

    // ---------------------------------- systemStatusTypeCode = 1 ----------------------------------------- //
    // -------- chooseProposalList -------- //
    this.route('chooseProposalList', {
        path: '/eleccion/propuestas',
        loginRequired: 'login',
        controller: chooseProposalController
    });

    this.route('chooseProposalListMostRecent', {
        path: '/eleccion/propuestas/mas-recientes',
        loginRequired: 'login',
        controller: chooseProposalController
    });

    this.route('chooseProposalListMostVoted', {
        path: '/eleccion/propuestas/mas-votadas',
        loginRequired: 'login',
        controller: chooseProposalController
    });

    // -------- chooseProposalRead -------- //
    this.route('chooseProposalRead', {
        path: '/eleccion/propuesta/:_id/leer',
        loginRequired: 'login',
        controller: chooseProposalController
    });

    // ---------------------------------- systemStatusTypeCode = 2 ----------------------------------------- //
    // -------- discussProposalRead -------- //
    this.route('discussProposalRead', {
        path: '/debate/propuesta/:_id/leer',
        loginRequired: 'login',
        controller: ProposalController
    });


    // -------------------------------------------- Route end ------------------------------------- //
});

