if (SystemStatusType.find().count() === 0) {
    var i = 1;

    SystemStatusType.insert({
        code: i,
        name: 'CHOICE_PROPOSAL',
        description: 'Elección de propuesta',
        durationInDays : 7
    });

    i++;
    SystemStatusType.insert({
        code: i,
        name: 'DEBATE',
        description: 'Debate',
        durationInDays: 0
    });

    i++;
    SystemStatusType.insert({
        code: i,
        name: 'CHOICE_PROMOTERS',
        description: 'Elección de promotores',
        durationInDays: 0
    });

    i++;
    SystemStatusType.insert({
        code: i,
        name: 'FIRM_PROPOSALS',
        description: 'Firma de propuestas',
        durationInDays: 0
    });

    i++;
    SystemStatusType.insert({
        code: i,
        name: 'IDLE_STATUS',
        description: 'Estado de espera',
        durationInDays: 0
    });
}

if (Meteor.users.find({username: settings.userAdmin.username}).count() === 0) {
    Accounts.createUser({
        username: settings.userAdmin.username,
        email: settings.userAdmin.email,
        password: settings.userAdmin.password
    });
    user_admin = Meteor.users.findOne({username: settings.userAdmin.username});
}


if (SystemStatus.find().count() === 0) {
    systemStatus.setChooseProposalStage();
}