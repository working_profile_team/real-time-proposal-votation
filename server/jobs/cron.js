/**
 *
 * minute          0-59
 * hour            0-23
 * day of month    1-31
 * month           1-12 (or names, see below)
 * day of week     0-7 (0 or 7 is Sun, or use names)
 *
 **/

var Jobs = {
    updateSystemStatus: function (lastSystemStatus) {
        if (lastSystemStatus.hasDuration){
            var currentDate = new Date();
            var deltaInSeconds = lastSystemStatus.endsAt - currentDate;
            if (deltaInSeconds < 0){
                var statusTypeCount = SystemStatusType.find().count();
                var nextStatusTypeCode = (lastSystemStatus.statusTypeCode + 1) % statusTypeCount;
                if (nextStatusTypeCode == 1)
                    systemStatus.setChooseProposalStage();
                if (nextStatusTypeCode == 2)
                    systemStatus.setDiscussProposalStage();
            }
        }

    },
};

new Meteor.Cron({
    events: {
        "* * * * *": function () {
            var lastSystemStatus = SystemStatus.findOne({}, {sort: {createdAt: -1}, limit: 1});
            Jobs.updateSystemStatus(lastSystemStatus);
        }
    }
});
