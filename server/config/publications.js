
Meteor.publish('proposals', function () {
    return Proposals.find();
});

Meteor.publish('articles', function () {
    return Articles.find();
});

Meteor.publish('systemStatus', function () {
    return SystemStatus.find();
});

Meteor.publish('systemStatusType', function () {
    return SystemStatusType.find();
});

Meteor.publish('systemParameters', function () {
    return SystemParameters.find();
});

Meteor.publish('proposalComments', function () {
    return ProposalComments.find();
});

Meteor.publish('proposalCommentResponses', function () {
    return ProposalCommentResponses.find();
});

Meteor.publish('discussProposalVotingSummary', function () {
    return DiscussProposalVotingSummary.find();
});

Meteor.publish('discussProposalVotingRecords', function () {
    return DiscussProposalVotingRecords.find();
});