Validations = {
    Proposals: {
        checkProposalAdd: function (user, proposalAttrs) {
            // ensure the user is logged in
            if (!user)
                throw new Meteor.Error(401, "Necesitas estar logueado para crear una propuesta");
            // ensure the proposal has a title
            if (!proposalAttrs.title)
                throw new Meteor.Error(422, 'Por favor completa el título de la propuesta');
            // ensure the proposal has a description
            if (!proposalAttrs.description)
                throw new Meteor.Error(422, 'Por favor completa la descripción de la propuesta');

            // Articles validations
            _.each(proposalAttrs.articles, function (item, index) {
                // ensure the proposal has a description
                if (!item.description)
                    throw new Meteor.Error(422, 'Por favor completa la descripción del artículo');
            });
        },
        checkProposalEdit: function (currentProposalId, user, proposalAttrs) {
            // ensure the user is logged in
            if (!user)
                throw new Meteor.Error(401, "Necesitas estar logueado para modificar una propuesta");
            if (!Permissions.Proposals.canEditProposal(currentProposalId))
                throw new Meteor.Error(401, "No posee permisos para modificar estos datos");
            // ensure the proposal has a title
            if (!proposalAttrs.title)
                throw new Meteor.Error(422, 'Por favor completa el título de la propuesta');
            // ensure the proposal has a description
            if (!proposalAttrs.description)
                throw new Meteor.Error(422, 'Por favor completa la descripción de la propuesta');

            // Articles validations
            _.each(proposalAttrs.articles, function (item, index) {
                // ensure the proposal has a description
                if (!item.description)
                    throw new Meteor.Error(422, 'Por favor completa la descripción del artículo');
            });
        },
        checkProposalDelete: function (currentProposalId, user) {
            // ensure the user is logged in
            if (!user)
                throw new Meteor.Error(401, "Necesitas estar logueado para modificar una propuesta");
            if (!Permissions.Proposals.canEditProposal(currentProposalId))
                throw new Meteor.Error(401, "No posee permisos para modificar estos datos");
        },
        checkProposalUpvote: function (user) {
            // ensure the user is logged in
            if (!user)
                throw new Meteor.Error(401, "Necesitas estar logueado para modificar una propuesta");
        },
    },
    ProposalComments: {
        checkProposalCommentAdd: function (currentProposalId, user, proposalCommentAttrs) {
            // ensure the user is logged in
            if (!user)
                throw new Meteor.Error(401, "Necesitas estar logueado para modificar una propuesta");
            if (!Permissions.ProposalComments.canAddProposalComment(currentProposalId))
                throw new Meteor.Error(401, "No posee permisos para modificar estos datos");
            // ensure the proposalComment has a description
            if (!proposalCommentAttrs.description)
                throw new Meteor.Error(422, 'Por favor completa la descripción del comentario antes de publicarlo');
        },
        checkProposalCommentDelete: function (currentProposalCommentId, user) {
            // ensure the user is logged in
            if (!user)
                throw new Meteor.Error(401, "Necesitas estar logueado para modificar una propuesta");
            if (!Permissions.ProposalComments.canEditProposalComment(currentProposalCommentId))
                throw new Meteor.Error(401, "No posee permisos para modificar estos datos");
        },
    },
    ProposalCommentResponses: {
        checkProposalCommentResponseAdd: function (currentProposalCommentId, user, proposalCommentResponseAttrs) {
            // ensure the user is logged in
            if (!user)
                throw new Meteor.Error(401, "Necesitas estar logueado para modificar una propuesta");
            if (!Permissions.ProposalCommentResponses.canAddProposalCommentResponse(currentProposalCommentId))
                throw new Meteor.Error(401, "No posee permisos para modificar estos datos");
            // ensure the proposalComment has a description
            if (!proposalCommentResponseAttrs.description)
                throw new Meteor.Error(422, 'Por favor completa la descripción de la respuesta antes de publicarla');
        },
        checkProposalCommentResponseDelete: function (currentProposalCommentResponseId, user) {
            // ensure the user is logged in
            if (!user)
                throw new Meteor.Error(401, "Necesitas estar logueado para modificar una propuesta");
            if (!Permissions.ProposalCommentResponses.canEditProposalCommentResponse(currentProposalCommentResponseId))
                throw new Meteor.Error(401, "No posee permisos para modificar estos datos");
        },
    }
};