/** Initialize all the collections that we need to use it **/
Proposals = new Meteor.Collection('proposals');

Articles = new Meteor.Collection('articles');

SystemStatus = new Meteor.Collection('systemStatus');
SystemStatusType = new Meteor.Collection('systemStatusType');

SystemParameters = new Meteor.Collection('systemParameters');

ProposalComments = new Meteor.Collection('proposalComments');
ProposalCommentResponses = new Meteor.Collection('proposalCommentResponses');

DiscussProposalVotingSummary = new Meteor.Collection('discussProposalVotingSummary');
DiscussProposalVotingRecords = new Meteor.Collection('discussProposalVotingRecords');