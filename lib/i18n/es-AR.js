/**es-AR**/
i18n.map('es-AR', {
    PROPOSAL: {
        PROPOSAL: 'Propuesta',
        ALL_PROPOSALS: 'Todas las propuestas',
        DESCRIPTION: 'Descripción de la propuesta',
        ARTICLES: 'Artículos',
        THIS_PROPOSAL_DOES_NOT_HAVE_ANY_ARTICLE: 'Esta propuesta no contiene artículos',
        SEE_FULL_TEXT: 'Ver texto completo',
        EDIT_PROPOSAL: 'Editar propuesta',
        SAVE_CHANGES: 'Guardar cambios',
        ADD_ARTICLE: 'Agregar artículo',
        TITLE: 'Título',
        WRITE_A_PROPOSAL_TITLE: 'Escriba un título para la propuesta ...',
        WRITE_A_PROPOSAL_DESCRIPTION: 'Escriba una descripción para la propuesta ...',
        NEW_PROPOSAL: 'Nueva propuesta',
        VOTE: {
            DISCUSS: 'Debatir',
            VOTE: 'Voto',
            MOST_VOTED: 'Más votadas',
            MOST_RECENT: 'Más recientes',
            ORDER_BY: 'Ordenar por'
        }
    },
    GLOBAL: {
        BACK_TO_HOMEPAGE: 'Volver a la página principal',
        HEADER: {
            SEE_PROPOSALS: 'Ver propuestas',
            CREATE_PROPOSALS: 'Crear propuestas',
            ENDS_IN: 'Termina en',
            DAY: 'día'
        }
    },
    ACTIONS: {
        EDIT: 'Editar',
        DELETE: 'Borrar',
    },
    ERRORS: {
        SORRY_YOU_CANNOT_EDIT_THIS_PROPOSAL: "No puedes editar esta propuesta",
        SORRY_YOU_DONT_HAVE_THE_RIGHTS_TO_VIEW_THIS_PAGE: "No tienes los permisos necesarios para ver esta página"
    },
    STAGE: {
        CHOOSE_OF_PROPOSAL: 'Elección de propuesta',
        DISCUSS_OF_PROPOSAL: 'Debate de propuesta'
    },
    PROPOSAL_COMMENTS: {
        POST: 'Publicar',
        WROTE: 'escribió',
        ANSWERED: 'respondió'
    }
});