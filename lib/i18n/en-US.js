/**en-US**/
i18n.map('en-US', {
    PROPOSAL: {
        SEE_PROPOSAL: 'See proposal',
        ALL_PROPOSALS: 'All proposals',
        DESCRIPTION: 'Proposal description',
        ARTICLES: 'Articles',
        THIS_PROPOSAL_DOES_NOT_HAVE_ANY_ARTICLE: 'This proposal does not have any article',
        SEE_FULL_TEXT: 'See full text',
        EDIT_PROPOSAL: 'Edit proposal',
        SAVE_CHANGES: 'Save changes',
        ADD_ARTICLE: 'Add article',
        TITLE: 'Title',
        WRITE_A_PROPOSAL_TITLE: 'Write a proposal title ...',
        WRITE_A_PROPOSAL_DESCRIPTION: 'Write a proposal description ...',
        NEW_PROPOSAL: 'New proposal',
        VOTE: {
            DISCUSS: 'Discuss',
            VOTE: 'Vote',
            MOST_VOTED: 'Most voted',
            MOST_RECENT: 'Most recent',
            ORDER_BY: 'Order by'
        }
    },
    GLOBAL: {
        BACK_TO_HOMEPAGE: 'Back to homepage',
        HEADER: {
            SEE_PROPOSALS: 'See proposals',
            CREATE_PROPOSALS: 'Create proposals',
            ENDS_IN: 'Ends in',
            DAY: 'day'
        }
    },
    ACTIONS: {
        EDIT: 'Edit',
        DELETE: 'Delete',
    },
    ERRORS: {
        SORRY_YOU_CANNOT_EDIT_THIS_PROPOSAL: "Sorry, you cannot edit this poposal.",
        SORRY_YOU_DONT_HAVE_THE_RIGHTS_TO_VIEW_THIS_PAGE: "Sorry, you don't have the rights to view this page."
    },
    STAGE: {
        CHOOSE_OF_PROPOSAL: 'Choose of proposal',
        DISCUSS_OF_PROPOSAL: 'Discuss of proposal'
    }
});