

Debugger = {
    init: function () {
//        if (Meteor.isClient) {
//            this.logRenders();
//        }
//        if (Meteor.isServer) {
//            this.logDatabase();
//        }
    },
    logWhichSide: function () {
        if (settings.debug) {
            if (Meteor.isServer) {
                console.log("is server")
            }
            if (Meteor.isClient) {
                console.log("is client")
            }
            console.log('isSimulation', this.isSimulation)
        }
    },
    logRenders: function () {
        if (settings.debug) {
            _.each(Template, function (template, name) {
                var oldRender = template.rendered;
                var counter = 0;
                template.rendered = function () {
                    console.log('[RENDER]', name, 'render count: ', ++counter);
                    oldRender && oldRender.apply(this, arguments);
                };
            });
        }
    },
    logDatabase: function () {
        if (settings.debug) {
            var wrappedFind = Meteor.Collection.prototype.find;

            Meteor.Collection.prototype.find = function () {
                var cursor = wrappedFind.apply(this, arguments);
                var collectionName = this._name;

                cursor.observeChanges({
                    added: function (id, fields) {
                        console.log(collectionName, 'added', id, fields);
                    },

                    changed: function (id, fields) {
                        console.log(collectionName, 'changed', id, fields);
                    },

                    movedBefore: function (id, before) {
                        console.log(collectionName, 'movedBefore', id, before);
                    },

                    removed: function (id) {
                        console.log(collectionName, 'removed', id);
                    }
                });

                return cursor;
            };
        }
    },
    log: function () {
        if (settings.debug) {
            console.log.apply(console, arguments);
        }
    }
}
