
DiscussProposalHelpers = {
    deleteLastVote: function (currentProposalId, user, userLastVote) {
        var discussProposalVotingSummaryData = {};

        if (userLastVote.votingCode == 1)
            discussProposalVotingSummaryData.$inc = {abstentionVotes: -1};
        if (userLastVote.votingCode == 2)
            discussProposalVotingSummaryData.$inc = {affirmativeVotes: -1};
        if (userLastVote.votingCode == 3)
            discussProposalVotingSummaryData.$inc = {negativeVotes: -1};
    //            if (votingCode == 4)
    //                discussProposalVotingSummaryData.$inc = {undecidedVotes: 1};
        DiscussProposalVotingSummary.update(
            {
                proposalId: currentProposalId,
            },
            discussProposalVotingSummaryData
        );

        DiscussProposalVotingRecords.remove({
            proposalId: currentProposalId,
            userId: user._id
        });
    },
    addProposalVotingSummary: function (currentProposalId, votingCode) {
        var discussProposalVotingSummaryData = {};
        if (votingCode == 1)
            discussProposalVotingSummaryData.$inc = {abstentionVotes: 1};
        if (votingCode == 2)
            discussProposalVotingSummaryData.$inc = {affirmativeVotes: 1};
        if (votingCode == 3)
            discussProposalVotingSummaryData.$inc = {negativeVotes: 1};
        DiscussProposalVotingSummary.update(
            {
                proposalId: currentProposalId,
            },
            discussProposalVotingSummaryData
        );
    },
    addProposalVotingRecord: function (currentProposalId, user, votingCode) {
        DiscussProposalVotingRecords.insert(
            {
                proposalId: currentProposalId,
                votingCode: votingCode,
                userId: user._id,
                author: user.username,
                createdAt: new Date().getTime(),
            }
        );
    },
};