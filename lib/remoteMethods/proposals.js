
Meteor.methods({
    proposalAdd: function(proposalAttrs) {
        var user = Meteor.user();
        Validations.Proposals.checkProposalAdd(user, proposalAttrs);
        // pick out the whitelisted keys
        var proposal = _.extend(_.pick(proposalAttrs, 'title', 'description'), {
            userId: user._id,
            author: user.username,
            createdAt: new Date().getTime(),
            lastModifiedBy: user._id,
            lastModifiedAt: new Date().getTime(),
            stageCode: 1,
        });
        var proposalId = Proposals.insert(proposal);

        // Inserto los artículos nuevos
        var articles = proposalAttrs.articles;
        _.each(articles, function (item, index) {
            var articleNumber = index + 1;
            var article = _.extend(_.pick(item, 'description'), {
                articleNumber: articleNumber,
                proposalId: proposalId,
                userId: user._id,
                author: user.username,
                createdAt: new Date().getTime()
            });
            Articles.insert(article);
        });

        return proposalId;
    },
    proposalEdit: function(currentProposalId, proposalAttrs) {
        var user = Meteor.user();
        Validations.Proposals.checkProposalEdit(currentProposalId, user, proposalAttrs);
        // pick out the whitelisted keys
        var proposal = _.extend(_.pick(proposalAttrs, 'title', 'description'), {
            lastModifiedBy: user._id,
            lastModifiedAt: new Date().getTime()
        });
        Proposals.update(currentProposalId, {$set: proposal});

        // Elimino artículos viejos e inserto los nuevos
        Articles.remove({proposalId: currentProposalId});
        var articles = proposalAttrs.articles;
        _.each(articles, function (item, index) {
            var articleNumber = index + 1;
            var article = _.extend(_.pick(item, 'description'), {
                articleNumber: articleNumber,
                proposalId: currentProposalId,
                userId: user._id,
                author: user.username,
                createdAt: new Date().getTime()
            });
            Articles.insert(article);
        });

        return currentProposalId;
    },
    proposalDelete: function(currentProposalId) {
        var user = Meteor.user();
        Validations.Proposals.checkProposalDelete(currentProposalId, user);
        Proposals.remove(currentProposalId);
        return currentProposalId;
    },
    proposalUpvoteToggle: function (currentProposalId) {
        var user = Meteor.user();
        Validations.Proposals.checkProposalUpvote(user);

        var userHasVoted = Proposals.findOne({
            _id: currentProposalId,
            upvoters: user._id
        });

        if (userHasVoted){
            Proposals.update(
                { _id: currentProposalId },
                {
                    $pull: {upvoters: user._id},
                    $inc: {votes: -1}
                }
            );
        }else{
            Proposals.update(
                { _id: currentProposalId },
                {
                    $addToSet: {upvoters: user._id},
                    $inc: {votes: 1}
                }
            );
        }
    },
});





