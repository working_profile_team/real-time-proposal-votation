Meteor.methods({
    proposalCommentAdd: function (currentProposalId, proposalCommentAttrs) {
        var user = Meteor.user();
        Validations.ProposalComments.checkProposalCommentAdd(currentProposalId, user, proposalCommentAttrs);

        ProposalComments.insert({
            proposalId: currentProposalId,
            userId: user._id,
            author: user.username,
            createdAt: new Date().getTime(),
            description: proposalCommentAttrs.description
        });

        return currentProposalId;
    },
    proposalCommentDelete: function (currentProposalCommentId) {
        var user = Meteor.user();
        Validations.ProposalComments.checkProposalCommentDelete(currentProposalCommentId, user);

        // Elimino respuestas relacionadas con los comentarios
        ProposalCommentResponses.remove({proposalCommentId: currentProposalCommentId});
        // Elimino el comentario
        ProposalComments.remove(currentProposalCommentId);
        return currentProposalCommentId;
    },
});