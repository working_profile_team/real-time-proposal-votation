PermissionsHelper = {
    canEdit: function (user, item) {
        user = (typeof user === 'undefined') ? Meteor.user() : user;

        if (!user || !item) {
            return false;
        }
        //noinspection RedundantIfStatementJS
        if (user._id !== item.userId) {
            return false;
        }
        return true;
    },
    currentUserCanEdit: function (item) {
        return PermissionsHelper.canEdit(Meteor.user(), item);
    }
};