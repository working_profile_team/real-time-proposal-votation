Permissions = {
    Proposals: {
        canEditProposal: function (proposalId) {
            var proposal = Proposals.findOne(proposalId);
            return (proposal.stageCode == 1 && PermissionsHelper.currentUserCanEdit(proposal));
        },
    },
    ProposalComments: {
        canEditProposalComment: function (proposalCommentId) {
            var proposalComment = ProposalComments.findOne(proposalCommentId);
            return (PermissionsHelper.currentUserCanEdit(proposalComment));
        },
        canAddProposalComment: function (proposalId) {
            var proposal = Proposals.findOne(proposalId);
            return Boolean(proposal);
        }
    },
    ProposalCommentResponses: {
        canEditProposalCommentResponse: function (proposalCommentResponseId) {
            var proposalCommentResponse = ProposalCommentResponses.findOne(proposalCommentResponseId);
            return (PermissionsHelper.currentUserCanEdit(proposalCommentResponse));
        },
        canAddProposalCommentResponse: function (proposalCommentId) {
            var proposalComment = ProposalComments.findOne(proposalCommentId);
            return Boolean(proposalComment);
        }
    }
};