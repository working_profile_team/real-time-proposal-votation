systemStatus = {
    setChooseProposalStage: function () {
        var user_admin = Meteor.users.findOne({username: settings.userAdmin.username});
        var statusType = SystemStatusType.findOne({code: 1});
        var now = new Date().getTime();
        SystemStatus.insert({
            userId: user_admin._id,
            author: user_admin.username,
            statusTypeId: statusType._id,
            statusTypeCode: statusType.code,
            createdAt: now,
            hasDuration: true,
            startsAt: now,
            endsAt: now + (1000 * 3600 * 24) * statusType.durationInDays
        });
    },
    setDiscussProposalStage: function () {
        var user_admin = Meteor.users.findOne({username: settings.userAdmin.username});
        var statusType = SystemStatusType.findOne({code: 2});
        var now = new Date().getTime();
        var proposal = Proposals.findOne({}, {sort: {votes: -1}});
        SystemStatus.insert({
            userId: user_admin._id,
            author: user_admin.username,
            statusTypeId: statusType._id,
            statusTypeCode: statusType.code,
            createdAt: now,
            hasDuration: false,
            proposalId: proposal._id,
        });

        DiscussProposalVotingSummary.insert(
            {
                proposalId: proposal._id,
                userId: user_admin._id,
                author: user_admin.username,
                createdAt: now,
                affirmativeVotes: 0,
                negativeVotes: 0,
                availableVotes: 0,
                abstentionVotes: 0,
                lastModifiedBy: user_admin._id,
                lastModifiedAt: new Date().getTime(),
            }
        );

        Proposals.update({_id: proposal._id}, {$set: {stageCode: 2}});
        Meteor.call('updateDiscussAvailableVotes');
    }
};